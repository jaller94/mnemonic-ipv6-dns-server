# Mnemonic IPv6 DNS Server

A DNS server to resolve mnemonic IPv6 addresses.

Read about the idea here: https://wiki.chrpaul.de/ip6-mnemonic

Stability: Alpha (last evaluated: 2020-03-06)

## Run it

```bash
# Install dependencies
npm install

# Test the installation
npm run test

# Start the DNS server on port 5353
npm run start
```

If you want to run it on the default DNS port 5353, you'll need to run the program as an administrator. You may also run it on a high port number that's not protected by your system like this:

```bash
PORT=53535 npm run start
```
