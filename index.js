const dns = require('dns2');
const { nameToIp } = require('./mnemonic-ipv6');

const server = dns.createUDPServer((request, send) => {
  const response = new dns.Packet.createResponseFromRequest(request);

  response.header.qr = 1;
  for (const question of request.questions) {
    const address = nameToIp(question.name);
    if (!address) continue;
    response.answers.push({
      name: question.name,
      address,
      type: dns.Packet.TYPE.AAAA,
      class: dns.Packet.CLASS.IN,
    });
  }

  send(response);
});

server.on('error', console.error);

server.listen(process.env.PORT || 5353);
