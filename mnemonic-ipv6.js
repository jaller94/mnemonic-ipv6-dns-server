const bip39 = require('bip39');

/**
 * Resolves a domain to IPv6 address.
 */
const nameToIp = (name) => {
  if (!name.endsWith('.ipv6')) return;
  const mnemonic = name.substring(0, name.length - 5).split('.').reverse().join(' ');
  let hex;
  try {
    hex = bip39.mnemonicToEntropy(mnemonic);
  } catch {
    return;
  }
  let ip = '';
  for (let i = 0; i < 8; i++) {
    ip += hex.substring(i * 4, i * 4 + 4) + ':';
  }
  return ip.substring(0, ip.length - 1);
};

/**
 * Generates a mnemonic domain from an IPv6 address.
 */
const ipToName = (ip) => {
  const hex = ipToHex(ip);
  const bip = bip39.entropyToMnemonic(hex);
  return bip.split(' ').reverse().join('.') + '.ipv6';
};

const count = (string, needle) => (
  (string.match(RegExp(needle, 'g')) || []).length
);

/**
 * Transforms an IPv6 address to a 32 bytes hex string.
 */
const ipToHex = (ip) => {
  ip = ip.replace('::', ':'.repeat(9 - count(ip, ':')));
  const groups = ip.split(':');
  return groups.map((section) => section.padStart(4, '0')).join('');
};

module.exports = {
  ipToHex,
  ipToName,
  nameToIp,
};
