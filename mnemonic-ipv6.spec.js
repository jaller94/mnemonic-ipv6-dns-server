const {
  ipToHex,
  ipToName,
  nameToIp,
} = require('./mnemonic-ipv6');

describe('ipToHex', () => {
  test('with no numbers being omitted', () => {
    expect(ipToHex('2a01:04f8:0c0c:2b18:0000:0000:0000:0002')).toBe('2a0104f80c0c2b180000000000000002');
  });
  test('with omitted numbers', () => {
    expect(ipToHex('2a01:4f8:c0c:2b18:0:0:0:2')).toBe('2a0104f80c0c2b180000000000000002');
  });
  test('with omitted groups', () => {
    expect(ipToHex('2a01:4f8:c0c:2b18::2')).toBe('2a0104f80c0c2b180000000000000002');
  });
  test('local ::1 address', () => {
    expect(ipToHex('::1')).toBe('00000000000000000000000000000001');
  });
  test('local :: address', () => {
    expect(ipToHex('::')).toBe('00000000000000000000000000000000');
  });
});

describe('ipToName', () => {
  test('test address 1', () => {
    expect(ipToName('2a01:04f8:0c0c:2b18:0000:0000:0000:0002')).toBe('agree.abandon.abandon.abandon.abandon.abandon.metal.section.blossom.dignity.amused.claw.ipv6');
  });
  test('test address ::', () => {
    expect(ipToName('::')).toBe('about.abandon.abandon.abandon.abandon.abandon.abandon.abandon.abandon.abandon.abandon.abandon.ipv6');
  });
  test('test address ::1', () => {
    expect(ipToName('::1')).toBe('actual.abandon.abandon.abandon.abandon.abandon.abandon.abandon.abandon.abandon.abandon.abandon.ipv6');
  });
});

describe('nameToIp', () => {
  test('test address 1', () => {
    expect(nameToIp('agree.abandon.abandon.abandon.abandon.abandon.metal.section.blossom.dignity.amused.claw.ipv6')).toBe('2a01:04f8:0c0c:2b18:0000:0000:0000:0002');
  });
  test('returns undefined for an invalid checksum', () => {
    expect(nameToIp('actual.abandon.abandon.abandon.abandon.abandon.metal.section.blossom.dignity.amused.claw.ipv6')).toBeUndefined();
  });
});
